Backend para la app de preguntas de UNNOBA.

Daddiego Lucas. Mendiola Julia. 2014



Si no se está logueado, lo único que se puede hacer es registrarse, para luego loguearse.



Para obtener los archivos es necesario seguir los siguientes sencillos pasos:
-Ingresar a la api correspondiente: .../archivos/{id}
-Tomar la url que provee la descripción del archivo, bajo el campo 'archivo'. Dicho campo tiene la ruta del archivo.
-Agregar dicha url al path correspondiente: /archivos/{url} donde {url} es el campo 'archivo', obtenido en el paso anterior.
Esto es posible ya que el proyecto está configurado de tal manera que cuando se recibe un requerimiento con URL
del tipo /archivos/ lo va a buscar a la carpeta archivos. Por lo tanto, para tomar un archivo desde un cliente,
basta tomar el campo mencionado ('archivo') de JSON, y crear la url: http://<servidor>/<contenido_campo_archivo>
y obtener el archivo.