Puntos de entradas de la api. Todo el intercambio de datos es mediante JSON.

/api/alumnos/
	GET: obtener todos los alumnos. Se necesita estar autenticado.
	POST: registrar alumno. Se necesita NO estar autenticado.
	
	{agregar ejemplos de JSON en todos.}
	
/api/alumnos/{id}/
	GET: obtener un alumno en particular. Se necesita estar autenticado.
	PUT: actualizar TODOS los datos del alumno. Se necesita estar autenticado como el alumno a actualizar.
	PATCH: actualizar ALGUNOS datos del alumno. Se necesita estar autenticado y ser uno mismo.
	POST: -
	DELETE: no se permite.
	
/api/alumnos/{id}/preguntas/
	GET: obtener todas las preguntas de un alumno en particular. Se necesita estar autenticado.
	POST: -

/api/alumnos/{id}/respuestas/
	GET: obtener todas las respuestas de un alumno en particular. Se necesita estar autenticado.
	POST: -

/api/preguntas/
	GET: obtener todas las preguntas. Se necesita estar autenticado.
	POST: crear pregunta nueva. Se necesita estar autenticado.

/api/preguntas/{id}/
	GET: obtener una pregunta en particular. Se necesita estar autenticado.
	PUT: no se permite.
	PATCH: no se permite.
	POST: -
	DELETE: no se permite.

/api/preguntas/{id}/respuestas/
	GET: obtener todas las respuestas de una pergunta en particular. Se necesita estar autenticado.
	POST: crear respuesta nueva a esa pregunta. Se necesita estar autenticado.

/api/preguntas/{id}/respuestas/{id}/
	GET: obtener una respuesta en particular, de una pregunta en particular. Se necesita estar autenticado.
	PUT: no se permite.
	PATCH: no se permite.
	POST: -
	DELETE: no se permite.

/api/preguntas/{id}/archivos/
	GET: obtener todos los archivos de una pregunta en particular. Se necesita estar autenticado.
	POST: subir un archivo nuevo a esa pregunta. Se necesita estar autenticado.

/api/preguntas/{id}/archivos/{id}/
	GET: obtener un archivo en particular, de una pregunta en particular. Se necesita estar autenticado.
	PUT: no se permite.
	PATCH: no se permite.
	POST: -
	DELETE: no se permite.

/api/preguntas/{id}/respuestas/{id}/archivos/
	GET: obtener todos los archivos de una respuesta en particular, de una pregunta en particular.
		Se necesita estar autenticado.
	POST: subir un archivo nuevo a esa respuesta. Se necesita estar autenticado.

/api/preguntas/{id}/respuestas/{id}/archivos/{id}/
	GET: obtener un archivo en particular, de una respuesta en particular, de una pregunta en particular.
		Se necesita estar autenticado.
	PUT: no se permite.
	PATCH: no se permite.
	POST: -
	DELETE: no se permite.

/api/suscripciones/
	GET: obtener todas las suscripciones de alumnos a preguntas. Se necesita estar autenticado.
	POST: crear una suscripción nueva del alumno autenticado a una pregunta. Se necesita estar autenticado.

/api/suscripciones/{id}/
	GET: obtener una suscripción en particular. Se necesita estar autenticado.
	PUT: actualizar TODOS los datos de la suscripción. Se necesita estar autenticado.
	PATCH: -
	POST: -
	DELETE: eliminar la suscripción.

Interfaz administrativa de django:

	Se accede a través de: /admin/

Acceso a archivos:

	Se accede a un archivo en particular a través de: /archivos/{url}

	Donde el parámetro {url} es la url del archivo, que se obtiene de los archivos de las preguntas y respuestas.
	Django sirve automáticamente el archivo a través de ese path.
