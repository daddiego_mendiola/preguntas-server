# -*- coding: utf-8 -*-
from rest_framework import permissions

#Custom permissions


class IsOwnerOrReadOnlyNoDeleteNoPutNoPatch(permissions.BasePermission):
    """
    Custom permission to only allow owners of an object to edit it.
    """

    def has_object_permission(self, request, view, obj):
        # Read permissions are allowed to any request,
        # so we'll always allow GET, HEAD or OPTIONS requests.

        if request.method in ["DELETE", "PUT", "PATCH"]:
            return False

        if request.method in permissions.SAFE_METHODS:
            return True

        # Write permissions are only allowed to the owner of the snippet.
        return obj.alumno == request.user


class ArchivoPregunta_IsOwnerOrReadOnlyNoDeleteNoPutNoPatch(
    permissions.BasePermission):
    """
    Custom permission to only allow owners of an object to edit it.
    """

    def has_object_permission(self, request, view, obj):
        # Read permissions are allowed to any request,
        # so we'll always allow GET, HEAD or OPTIONS requests.

        if request.method in ["DELETE", "PUT", "PATCH"]:
            return False

        if request.method in permissions.SAFE_METHODS:
            return True

        # Write permissions are only allowed to the owner of the snippet.
        return obj.pregunta.alumno == request.user


class ArchivoRespuesta_IsOwnerOrReadOnlyNoDeleteNoPutNoPatch(
    permissions.BasePermission):
    """
    Custom permission to only allow owners of an object to edit it.
    """

    def has_object_permission(self, request, view, obj):
        # Read permissions are allowed to any request,
        # so we'll always allow GET, HEAD or OPTIONS requests.

        if request.method in ["DELETE", "PUT", "PATCH"]:
            return False

        if request.method in permissions.SAFE_METHODS:
            return True

        # Write permissions are only allowed to the owner of the snippet.
        return obj.respuesta.alumno == request.user


class IsOwnerOrReadOnly(permissions.BasePermission):
    """
    Custom permission to only allow owners of an object to edit it.
    """

    def has_object_permission(self, request, view, obj):
        # Read permissions are allowed to any request,
        # so we'll always allow GET, HEAD or OPTIONS requests.
        if request.method in permissions.SAFE_METHODS:
            return True

        # Write permissions are only allowed to the owner of the snippet.
        return obj.alumno == request.user


class Alumno_IsCreationOrIsAuthenticated(permissions.BasePermission):
    """
    Custom permission to only let an un-registered user register itself,
    and not retrieve all the user list. And, if a user is registered, be
    dont wanna to register itself again.
    """

    def has_permission(self, request, view):
        """
        Return `True` if permission is granted, `False` otherwise.
        """
        if (request.method in ["POST"] and
                not(request.user.is_authenticated())):
            return True
        else:
            if ((request.method in ["GET", "PUT", "PATCH"])
                and (request.user.is_authenticated())):
                return True
            else:
                return False

    def has_object_permission(self, request, view, obj):

        if ((request.method in ["GET"]) and (request.user.is_authenticated())):
            return True
        else:
            if ((request.method in ["PUT", "PATCH"]) and
                (request.user.is_authenticated()) and (request.user == obj)):
                return True
            else:
                return False