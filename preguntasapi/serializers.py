# -*- coding: utf-8 -*-

from rest_framework import serializers
from preguntasapp.models import *


class CarreraSerializer(serializers.ModelSerializer):
    class Meta:
        model = Carrera

class AlumnoSerializer(serializers.ModelSerializer):
    #preguntas = serializers.PrimaryKeyRelatedField(many=True)
    carrera = serializers.SlugRelatedField("carrera", many=False, slug_field='nombre', read_only=False)

    class Meta:
        model = Alumno
        fields = ('id', 'username', 'password', 'email', 'first_name',
        'last_name', 'dni', 'puntaje', 'suscripciones', 'carrera')
        read_only_fields = ('id',)
        write_only_fields = ('password',)


class LoginSerializer(serializers.ModelSerializer):
    class Meta:
        model = Alumno
        fields = ('id', 'username',)
        read_only_fields = ('id',)


class PreguntaSerializer(serializers.ModelSerializer):
    #alumno = serializers.RelatedField(many=False)
    alumno = AlumnoSerializer(read_only=True)
    fecha = serializers.DateTimeField(format="%d-%m-%Y %H:%M", read_only=True)
    #respuestas = serializers.PrimaryKeyRelatedField(many=True, read_only=True)
    cantidad_respuestas = serializers.IntegerField(source='respuestas.count',
        read_only=True)
    suscriptos = serializers.IntegerField(source='suscripcion_set.count',
        read_only=True)
    archivos = serializers.IntegerField(source='archivopregunta_set.count',
        read_only=True)

    class Meta:
        model = Pregunta
        depth = 1
        read_only_fields = ('id', 'puntaje', 'reportada',)
        fields = ('id', 'titulo', 'descripcion', 'alumno', 'puntaje', 'fecha',
            'reportada', 'suscriptos', 'archivos', 'cantidad_respuestas')


class RespuestaSerializer(serializers.ModelSerializer):
    #pregunta = serializers.PrimaryKeyRelatedField(many=False)
    #alumno = serializers.PrimaryKeyRelatedField(many=False)
    alumno = AlumnoSerializer(read_only=True)
    fecha = serializers.DateTimeField(format="%d-%m-%Y %H:%M", read_only=True)

    class Meta:
        model = Respuesta
        depth = 1
        read_only_fields = ('id', 'puntaje', 'reportada', 'pregunta',)
        fields = ('id', 'descripcion', 'alumno', 'fecha', 'puntaje',
            'reportada')


class SuscripcionSerializer(serializers.ModelSerializer):
    #alumno = serializers.PrimaryKeyRelatedField(many=False)
    pregunta = serializers.PrimaryKeyRelatedField(many=False)

    class Meta:
        model = Suscripcion
        read_only_fields = ('alumno',)


class ArchivoPreguntaSerializer(serializers.ModelSerializer):
    #pregunta = serializers.PrimaryKeyRelatedField(many=False)

    class Meta:
        model = ArchivoPregunta
        read_only_fields = ('pregunta',)


class ArchivoRespuestaSerializer(serializers.ModelSerializer):
    #respuesta = serializers.PrimaryKeyRelatedField(many=False)

    class Meta:
        model = ArchivoRespuesta
        read_only_fields = ('respuesta',)


class ReportePreguntaSerializer(serializers.ModelSerializer):

    class Meta:
        model = ReportePregunta
        read_only_fields = ('alumno', 'pregunta')


class ReporteRespuestaSerializer(serializers.ModelSerializer):

    class Meta:
        model = ReporteRespuesta
        read_only_fields = ('alumno', 'respuesta')