from django.conf.urls import patterns, url, include
from rest_framework.routers import DefaultRouter
from rest_framework_nested import routers

from preguntasapi import views

router2 = DefaultRouter()

# Create a router and register our viewsets with it.

router = routers.SimpleRouter()
router.register(r'preguntas', views.PreguntaViewSet)
router.register(r'alumnos', views.AlumnoViewSet)
router.register(r'login', views.LoginViewSet)
#router.register(r'archivos_pregunta', views.ArchivoPreguntaViewSet)
#router.register(r'archivos_respuesta', views.ArchivoRespuestaViewSet)
router.register(r'suscripciones', views.SuscripcionViewSet)
router.register(r'carreras', views.CarreraViewSet)

# Router anidados para preguntas.
preguntas_router = routers.NestedSimpleRouter(router, r'preguntas',
    lookup='pregunta')
preguntas_router.register(r'respuestas', views.RespuestaViewSet)
preguntas_router.register(r'archivos', views.ArchivoPreguntaViewSet)

# Router anidado para respuesta, a su vez anidado a preguntas.

respuestas_router = routers.NestedSimpleRouter(preguntas_router, r'respuestas',
    lookup='respuesta')
respuestas_router.register(r'archivos', views.ArchivoRespuestaViewSet)


# Router anidado para alumnos.
alumnos_router = routers.NestedSimpleRouter(router, r'alumnos',
    lookup='alumno')
alumnos_router.register(r'preguntas', views.VistaPreguntasDeAlumnoViewSet)
alumnos_router.register(r'respuestas', views.VistaRespuestasDeAlumnoViewSet)


# The API URLs are now determined automatically by the router.
# Additionally, we include the login URLs for the browseable API.
urlpatterns = patterns('',
    url(r'^', include(router.urls)),
    url(r'^', include(preguntas_router.urls)),
    url(r'^', include(respuestas_router.urls)),
    url(r'^', include(alumnos_router.urls)),
    url(r'^api-auth/', include('rest_framework.urls',
        namespace='rest_framework'))
)
