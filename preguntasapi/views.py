from django.contrib.auth.hashers import make_password
from rest_framework import viewsets, status
from rest_framework.permissions import *
from rest_framework.decorators import link, action
from rest_framework.response import Response

from preguntasapi.serializers import *
from preguntasapi.permissions import *
from preguntasapp.models import *
from django.db.models import Count

# Vistas de los modelos.

class CarreraViewSet(viewsets.ReadOnlyModelViewSet):
    queryset = Carrera.objects.all()
    serializer_class = CarreraSerializer
    permission_classes = []


class AlumnoViewSet(viewsets.ModelViewSet):
    """
    A simple ViewSet for viewing and editing accounts.
    """
    queryset = Alumno.objects.all()
    serializer_class = AlumnoSerializer
    permission_classes = [Alumno_IsCreationOrIsAuthenticated]

    def pre_save(self, obj):
        obj.password = make_password(obj.password)


class LoginViewSet(viewsets.ReadOnlyModelViewSet):
    """
    Login View
    """
    model = Alumno
    serializer_class = LoginSerializer
    permission_classes = [IsAuthenticated, ]

    def get_queryset(self):
        return [self.request.user]


class PreguntaViewSet(viewsets.ModelViewSet):
    """
    Vista para ver y crear preguntas.
    """
    model = Pregunta
    queryset = Pregunta.objects.annotate(num_respuestas=Count('respuestas')).order_by('num_respuestas')
    #queryset = Pregunta.objects.all()
    serializer_class = PreguntaSerializer
    permission_classes = [IsAuthenticated,
        IsOwnerOrReadOnlyNoDeleteNoPutNoPatch]

    def pre_save(self, obj):
        obj.alumno = self.request.user

    def post_save(self, obj, created):
        suscripcion = Suscripcion()
        suscripcion.alumno = self.request.user
        suscripcion.pregunta = obj
        suscripcion.save()

    def _votar(self, alumno, pregunta, valor_voto):
        if (pregunta.alumno == alumno):
            return Response({'status': "No se puede votar a si mismo."},
                status=status.HTTP_403_FORBIDDEN)
        try:
            voto = VotoPregunta()
            voto.alumno = alumno
            voto.pregunta = pregunta
            voto.voto = valor_voto
            voto.save()
            pregunta.puntaje += valor_voto
            pregunta.save()
            return Response({'status': "Voto realizado existosamente"},
                status=status.HTTP_200_OK)
        except:
            return Response({'status': "Ud. ya ha votado esta pregunta"},
                status=status.HTTP_403_FORBIDDEN)

    @link(permission_classes=[IsAuthenticated])
    def votar_positivo(self, request, pk=None):
        if (pk is None):
            return False

        pregunta = Pregunta.objects.get(pk=pk)
        alumno = self.request.user

        return self._votar(alumno, pregunta, 1)

    @link(permission_classes=[IsAuthenticated])
    def votar_negativo(self, request, pk=None):
        if (pk is None):
            return False

        pregunta = Pregunta.objects.get(pk=pk)
        alumno = self.request.user

        return self._votar(alumno, pregunta, -1)

    @action(permission_classes=[IsAuthenticated])
    def reportar(self, request, pk=None):
        pregunta = self.get_object()
        alumno = request.user
        if (pregunta.alumno == alumno):
            return Response({'status': "No se puede reportar a si mismo."},
                status=status.HTTP_403_FORBIDDEN)

        serializer = ReportePreguntaSerializer(data=request.DATA)

        if serializer.is_valid():
            reporte = ReportePregunta()
            reporte.alumno = alumno
            reporte.pregunta = pregunta
            reporte.descripcion = serializer.data["descripcion"]
            try:
                reporte.save()
                pregunta.reportada += 1
                pregunta.save()
                return Response({'status': 'Pregunta reportada exitosamente'})
            except:
                return Response({'status': "Ud. ya ha votado esta pregunta"},
                status=status.HTTP_403_FORBIDDEN)
        else:
            return Response(serializer.errors,
                            status=status.HTTP_400_BAD_REQUEST)


class SuscripcionViewSet(viewsets.ModelViewSet):
    """
    Vista para ver y crear/editar preguntas.
    """
    model = Suscripcion
    serializer_class = SuscripcionSerializer
    permission_classes = [IsAuthenticated, IsOwnerOrReadOnly]

    def pre_save(self, obj):
        obj.alumno = self.request.user

    def get_queryset(self):
        return self.request.user.suscripcion_set.all()


# Vistas Anidadas


class RespuestaViewSet(viewsets.ModelViewSet):
    """
    Vista para ver y crear/editar respuestas.
    """
    model = Respuesta
    #queryset = Respuesta.objects.all()
    serializer_class = RespuestaSerializer
    permission_classes = [IsAuthenticated,
        IsOwnerOrReadOnlyNoDeleteNoPutNoPatch]

    def get_queryset(self):
        pregunta_id = self.kwargs['pregunta_pk']
        return Respuesta.objects.filter(pregunta=pregunta_id)

    def pre_save(self, obj):
        obj.pregunta = Pregunta.objects.get(pk=self.kwargs['pregunta_pk'])
        obj.alumno = self.request.user

    def _votar(self, alumno, respuesta, valor_voto):
        if (respuesta.alumno == alumno):
            return Response({'status': "No se puede votar a si mismo."},
                status=status.HTTP_403_FORBIDDEN)
        try:
            voto = VotoRespuesta()
            voto.alumno = alumno
            voto.respuesta = respuesta
            voto.voto = valor_voto
            voto.save()
            respuesta.puntaje += valor_voto
            respuesta.save()
            return Response({'status': "Voto realizado existosamente"},
                status=status.HTTP_200_OK)
        except:
            return Response({'status': "Ud. ya ha votado esta pregunta"},
                status=status.HTTP_403_FORBIDDEN)

    @link(permission_classes=[IsAuthenticated])
    def votar_positivo(self, request, pk=None, pregunta_pk=None):
        if (pk is None):
            return False

        respuesta = Respuesta.objects.get(pk=pk)
        alumno = self.request.user

        return self._votar(alumno, respuesta, 1)

    @link(permission_classes=[IsAuthenticated])
    def votar_negativo(self, request, pk=None, pregunta_pk=None):
        if (pk is None):
            return False

        respuesta = Respuesta.objects.get(pk=pk)
        alumno = self.request.user

        return self._votar(alumno, respuesta, -1)

    @action(permission_classes=[IsAuthenticated])
    def reportar(self, request, pk=None, pregunta_pk=None):
        respuesta = self.get_object()
        alumno = request.user
        if (respuesta.alumno == alumno):
            return Response({'status': "No se puede reportar a si mismo."},
                status=status.HTTP_403_FORBIDDEN)

        serializer = ReporteRespuestaSerializer(data=request.DATA)

        if serializer.is_valid():
            reporte = ReporteRespuesta()
            reporte.alumno = alumno
            reporte.respuesta = respuesta
            reporte.descripcion = serializer.data["descripcion"]
            try:
                reporte.save()
                respuesta.reportada += 1
                respuesta.save()
                return Response({'status': 'Respuesta reportada exitosamente'})
            except:
                return Response({'status': "Ud. ya ha votado esta pregunta"},
                status=status.HTTP_403_FORBIDDEN)
        else:
            return Response(serializer.errors,
                            status=status.HTTP_400_BAD_REQUEST)


class ArchivoPreguntaViewSet(viewsets.ModelViewSet):
    """
    A simple ViewSet for viewing and editing accounts.
    """
    model = ArchivoPregunta
    serializer_class = ArchivoPreguntaSerializer
    permission_classes = [IsAuthenticated,
        ArchivoPregunta_IsOwnerOrReadOnlyNoDeleteNoPutNoPatch]

    def get_queryset(self):
        pregunta_id = self.kwargs['pregunta_pk']
        return ArchivoPregunta.objects.filter(pregunta=pregunta_id)

    def pre_save(self, obj):
        obj.pregunta = Pregunta.objects.get(pk=self.kwargs['pregunta_pk'])


class ArchivoRespuestaViewSet(viewsets.ModelViewSet):
    """
    A simple ViewSet for viewing and editing accounts.
    """
    model = ArchivoRespuesta
    serializer_class = ArchivoRespuestaSerializer
    permission_classes = [IsAuthenticated,
        ArchivoRespuesta_IsOwnerOrReadOnlyNoDeleteNoPutNoPatch]

    def get_queryset(self):
        respuesta_id = self.kwargs['respuesta_pk']
        return ArchivoRespuesta.objects.filter(respuesta=respuesta_id)

    def pre_save(self, obj):
        obj.respuesta = Respuesta.objects.get(pk=self.kwargs['respuesta_pk'])


# Vistas anidadas especiales
class VistaRespuestasDeAlumnoViewSet(viewsets.ReadOnlyModelViewSet):
    """
    Vista para ver las preguntas de un alumno en particular
    usando alumnos/id/preguntas
    """
    model = Respuesta
    serializer_class = RespuestaSerializer
    permission_classes = [IsAuthenticated]

    def get_queryset(self):
        alumno_id = self.kwargs['alumno_pk']
        queryset = Respuesta.objects.filter(alumno=alumno_id)
        return queryset


class VistaPreguntasDeAlumnoViewSet(viewsets.ReadOnlyModelViewSet):
    """
    Vista para ver las preguntas de un alumno en particular
    usando alumnos/id/preguntas
    """
    model = Pregunta
    serializer_class = PreguntaSerializer
    permission_classes = [IsAuthenticated]

    def get_queryset(self):
        alumno_id = self.kwargs['alumno_pk']
        queryset = Pregunta.objects.filter(alumno=alumno_id)
        return queryset