from django.contrib import admin
from django.contrib.auth.admin import UserAdmin
from django.contrib.auth.forms import UserChangeForm, UserCreationForm
from django import forms

#Our app models
from preguntasapp.models import *


# This class is for the Django Admin app, so our Alumno model
# shows well.

class AlumnoAdmin(UserAdmin):

    class AlumnoChangeForm(UserChangeForm):
        class Meta(UserChangeForm.Meta):
            model = Alumno

    class AlumnoCreationForm(UserCreationForm):
        class Meta(UserCreationForm.Meta):
            model = Alumno

        def clean_username(self):
            username = self.cleaned_data['username']
            try:
                Alumno.objects.get(username=username)
            except Alumno.DoesNotExist:
                return username
            raise forms.ValidationError(
                    self.error_messages['duplicate_username'])

    form = AlumnoChangeForm
    add_form = AlumnoCreationForm
    fieldsets = (("Alumno Model Info", {'fields': Alumno.FIELDS}),) \
                + UserAdmin.fieldsets
    add_fieldsets = UserAdmin.add_fieldsets + (("Alumno Model Info",
                {'fields': Alumno.FIELDS}),)

# Here we register our models into the Django Admin Interface
admin.site.register(Alumno, AlumnoAdmin)
admin.site.register(Pregunta)
admin.site.register(Suscripcion)
admin.site.register(Respuesta)
admin.site.register(ArchivoRespuesta)
admin.site.register(ArchivoPregunta)
admin.site.register(VotoPregunta)
admin.site.register(VotoRespuesta)
admin.site.register(ReportePregunta)
admin.site.register(ReporteRespuesta)
admin.site.register(Carrera)

# We could tweak it much more, for example, putting the files in InlineForms.