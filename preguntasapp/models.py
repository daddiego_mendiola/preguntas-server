# -*- coding: utf-8 -*-
from django.db import models
from django.contrib.auth.models import AbstractUser
from django.db.models import Count


class Carrera(models.Model):
    nombre = models.CharField(max_length=80, null=False, unique=True)

    class Meta:
        ordering = ('nombre',)

    def __str__(self):
        return self.nombre


#We subclass AbstracUser from Django, so we have all the auth.
class Alumno(AbstractUser):
    dni = models.CharField(max_length=10, null=False, unique=True)
    puntaje = models.IntegerField(default=0)
    carrera = models.ForeignKey(Carrera, null=True)

    #Put here the custom fields
    FIELDS = ('dni', 'carrera',)

    def __str__(self):
        return self.username + ": " + self.last_name + " " + self.first_name

    class Meta(AbstractUser.Meta):
        verbose_name = 'alumno'
        verbose_name_plural = 'alumnos'
        swappable = 'AUTH_USER_MODEL'


class Pregunta(models.Model):
    titulo = models.CharField(max_length=200)
    descripcion = models.TextField()
    puntaje = models.IntegerField(default=0)
    reportada = models.IntegerField(default=0)
    fecha = models.DateTimeField(auto_now_add=True)

    #Relaciones
    alumno = models.ForeignKey(Alumno, related_name='preguntas')
    suscripciones = models.ManyToManyField(Alumno, through="Suscripcion",
        related_name='suscripciones')
    votos = models.ManyToManyField(Alumno, through="VotoPregunta",
        related_name='preguntas_votadas')
    reportes = models.ManyToManyField(Alumno, through="ReportePregunta",
        related_name="preguntas_reportadas")

    # On Python 3: def __str__(self):
    def __str__(self):
        return self.titulo

    class Meta:
        ordering = ('-fecha',)
        verbose_name = 'pregunta'
        verbose_name_plural = 'preguntas'


class Suscripcion(models.Model):
    alumno = models.ForeignKey(Alumno)
    pregunta = models.ForeignKey(Pregunta)
    fecha = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return str(self.alumno) + " : " + str(self.pregunta)

    class Meta:
        ordering = ('fecha',)
        verbose_name = 'suscripcion'
        verbose_name_plural = 'suscripciones'
        unique_together = ('alumno', 'pregunta',)


class Respuesta(models.Model):
    descripcion = models.TextField()
    puntaje = models.IntegerField(default=0)
    reportada = models.IntegerField(default=0)
    fecha = models.DateTimeField(auto_now_add=True)

    #Relaciones
    pregunta = models.ForeignKey(Pregunta, related_name='respuestas')
    alumno = models.ForeignKey(Alumno)
    votos = models.ManyToManyField(Alumno, through="VotoRespuesta",
        related_name='respuestas_votadas')
    reportes = models.ManyToManyField(Alumno, through="ReporteRespuesta",
        related_name="respuestas_reportadas")

    def __str__(self):
        return str(self.alumno) + " : " + self.descripcion[:50]

    class Meta:
        ordering = ('-puntaje', 'fecha',)
        verbose_name = 'respuesta'
        verbose_name_plural = 'respuestas'


class ArchivoPregunta(models.Model):
    archivo = models.FileField(upload_to="preguntas/%Y/%m/%d")
    pregunta = models.ForeignKey(Pregunta)

    def __str__(self):
        return str(self.pregunta) + " : " + self.archivo.name


class ArchivoRespuesta(models.Model):
    archivo = models.FileField(upload_to="respuestas/%Y/%m/%d")
    respuesta = models.ForeignKey(Respuesta)

    def __str__(self):
        return str(self.respuesta) + " : " + self.archivo.name


class VotoPregunta(models.Model):
    pregunta = models.ForeignKey(Pregunta)
    alumno = models.ForeignKey(Alumno)
    voto = models.IntegerField()
    fecha = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return str(self.alumno) + " " + str(self.pregunta) + " " + \
            str(self.voto)

    class Meta:
        unique_together = ('alumno', 'pregunta',)


class VotoRespuesta(models.Model):
    respuesta = models.ForeignKey(Respuesta)
    alumno = models.ForeignKey(Alumno)
    voto = models.IntegerField()
    fecha = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return str(self.alumno) + " " + str(self.respuesta) + " " + \
            str(self.voto)

    class Meta:
        unique_together = ('alumno', 'respuesta',)


class ReportePregunta(models.Model):
    pregunta = models.ForeignKey(Pregunta)
    alumno = models.ForeignKey(Alumno)
    descripcion = models.TextField()
    fecha = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return str(self.alumno) + ": " + str(self.pregunta) + ": " + \
            str(self.pregunta.alumno) + ": " + str(self.descripcion)

    class Meta:
        unique_together = ('alumno', 'pregunta',)


class ReporteRespuesta(models.Model):
    respuesta = models.ForeignKey(Respuesta)
    alumno = models.ForeignKey(Alumno)
    descripcion = models.TextField()
    fecha = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return str(self.alumno) + ": " + str(self.respuesta) + ": " + \
            str(self.respuesta.alumno) + ": " + str(self.descripcion)

    class Meta:
        unique_together = ('alumno', 'respuesta',)